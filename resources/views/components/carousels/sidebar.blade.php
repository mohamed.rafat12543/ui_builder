<div class="sidebar-products-carousel">
    <div class="owl-carousel" id="sidebar-products-carousel">
        @include('partials.products.product_list_tile')
        @include('partials.products.product_list_tile')
        @include('partials.products.product_list_tile')
    </div>
</div>
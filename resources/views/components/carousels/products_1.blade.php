<div class="top-products-carousel">
    <div class="owl-carousel" id="top-products-carousel">
        @include('partials/products/product')
        @include('partials/products/product')
        @include('partials/products/product')
        @include('partials/products/product')
        @include('partials/products/product')
        @include('partials/products/product')
        @include('partials/products/product')
    </div>
</div>
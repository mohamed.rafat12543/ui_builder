<div class="product-list">
    <a class="btn btn-theme btn-title-more" href="#">See All</a>
    <h4 class="block-title"><span>Top Sellers</span></h4>
    @include('partials.products.product_list_tile')
    @include('partials.products.product_list_tile')
    @include('partials.products.product_list_tile')
</div>
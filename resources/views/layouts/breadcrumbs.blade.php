<!-- BREADCRUMBS -->
<section class="page-section breadcrumbs">
    <div class="container">
        <div class="page-header">
            <h1>Category Page</h1>
        </div>
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Shop</a></li>
            <li class="active">Category Grid View Page With Left Sidebar</li>
        </ul>
    </div>
</section>
<!-- /BREADCRUMBS -->
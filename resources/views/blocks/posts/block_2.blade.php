<section class="page-section">
    <div class="container">
        <a class="btn btn-theme btn-title-more btn-icon-left" href="#"><i class="fa fa-file-text-o"></i>See All Posts</a>
        <h2 class="block-title"><span>Our Recent posts</span></h2>
        <div class="row">
            <div class="col-md-4">
                @include('partials.posts.post_2')
            </div>
            <div class="col-md-4">
                @include('partials.posts.post_2')
            </div>
            <div class="col-md-4">
                @include('partials.posts.post_2')
            </div>
        </div>
    </div>
</section>
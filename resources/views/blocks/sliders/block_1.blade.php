<!-- PAGE slider-->
<section class="page-section no-padding slider">
    <div class="container full-width">

        <div class="main-slider">
            @include('components.carousels.main_slider')
        </div>

    </div>
</section>
<!-- /PAGE -->
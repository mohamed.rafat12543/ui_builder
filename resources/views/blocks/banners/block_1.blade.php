<section class="page-section no-padding-top">
    <div class="container">
        <div class="row blocks shop-info-banners">
            <div class="col-md-4">
                @include('components.banners.info_banner')
            </div>
            <div class="col-md-4">
                @include('components.banners.info_banner')
            </div>
            <div class="col-md-4">
                @include('components.banners.info_banner')
            </div>
        </div>
    </div>
</section>
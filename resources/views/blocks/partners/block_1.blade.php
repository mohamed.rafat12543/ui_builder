<!-- PAGE -->
<section class="page-section">
    <div class="container">
        <h2 class="section-title"><span>Brand &amp; Clients</span></h2>
        @includ('components.carousels.partners')
    </div>
</section>
<!-- /PAGE -->
<section class="page-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3">
               @include('partials.categories.category')
            </div>
            <div class="col-sm-6 col-md-3">
                @include('partials.categories.category')

            </div>
            <div class="col-sm-6 col-md-3">
               @include('partials.categories.category')
                
            </div>
            <div class="col-sm-6 col-md-3">
               @include('partials.categories.category')
                
            </div>
        </div>
    </div>
</section>
<!-- PAGE -->
<section class="page-section">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                @include('components.products.products_list')
            </div>
            <div class="col-md-4">
                @include('components.products.products_list')
            </div>
            <div class="col-md-4">
                @include('components.products.products_list')
            </div>
        </div>
    </div>
</section>
<!-- /PAGE -->
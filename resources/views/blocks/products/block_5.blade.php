<!-- PAGE -->
<section class="page-section">
    <div class="container">
        <h2 class="section-title"><span>Top Rated</span></h2>
        @include("components.carousels.products_1")
    </div>
</section>
<!-- /PAGE -->